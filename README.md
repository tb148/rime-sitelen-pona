# rime-sitelen-pona

This is a sitelen pona UCSUR IME based on rime.

Use `stl_pon` to type 3-letter word abbreviations.

## Special Characters

Use `-` or `+` to combine words into compounds.

Use space to type full-width spaces.

Use `<` and `>` to type CJK quotes.

Use `` ` ``  and `~` to type alternate glyphs.

Use `^` to combine words into stacked compounds.

Use `*` to combine words into scaled compounds.

Use `[` and `]` to make a cartouche.

Use `(` and `)` to extend a glyph to the right.

Use `{` and `}` to extend a glyph to the left.

Use `.` to type a middle dot.

Use `:` to type a colon.
